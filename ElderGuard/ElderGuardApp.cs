﻿using System.Runtime.Versioning;
using Autofac;
using ElderGuard.Container;
using ElderGuard.Concretes;
using ElderGuard.Services;
using ElderGuard.Plugins;
using ElderGuard.Core.PluginInfrastructure;
using ElderGuard.Concretes.PluginInfrastructure;
using ElderGuard.Core;
using Avalonia;
using Avalonia.Controls;

namespace ElderGuard
{
    /// <summary>
    /// The main app instance.
    /// </summary>
    public class ElderGuardApp
    {
        /// <summary>
        /// The container instance.
        /// </summary>
        protected ElderGuardContainer container { get; set; }

        /// <summary>
        /// The instance of the app built using avalonia.
        /// </summary>
        protected AppBuilder BuiltUIInstance;

        /// <summary>
        /// The parameters received when launching the app.
        /// </summary>
        protected string[] incomingParams;
        
        /// <summary>
        /// Runs the main app for the Elderguard instance.
        /// </summary>
        protected void RunElderGuardApp()
        { 
            using (var scope = container.useContainer().BeginLifetimeScope())
            {
                var systemTray = scope.Resolve<ElderGuardSystemTray>();
                var isTeamViewerInstalled = scope.Resolve<ElderGuardSoftwareManager>().refreshInstalledSoftware().isSoftwareInstalled("TeamViewer");
                var chromey = scope.Resolve<ElderGuardChromeAntiScamPlugin>();
                chromey.loadConfiguration(
                    new ElderGuardPluginConfigItem(
                        "ElderGuardChromeAntiScamPlugin",
                        new { ShouldDisableDevTools = true }
                    )
                );

                var commandy = scope.Resolve<ElderGuardConsoleAntiScamPlugin>();
                commandy.loadConfiguration(new ElderGuardPluginConfigItem("ElderGuardCommandAntiScamPlugin", new { ShouldForceCloseCMD = true }));
            }
        }

        /// <summary>
        /// The application does its own stuff.
        /// </summary>
        public ElderGuardApp(AppBuilder appBuilder, string[] args)
        {
            BuiltUIInstance = appBuilder;
            incomingParams = args;
            container = new ElderGuardContainer(BuiltUIInstance);
            RunElderGuardApp();
        }
    }
}
