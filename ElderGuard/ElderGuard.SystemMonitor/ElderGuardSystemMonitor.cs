﻿using ElderGuard.PluginInterOp.Interfaces;
using ElderGuard.PluginInterOp.Concretes;
using System;
using System.Collections.Generic;
using System.Composition;

namespace ElderGuard.SystemMonitor
{
    [Export(typeof(IElderGuardPlugin))]
    public class ElderGuardSystemMonitor : ElderGuardPlugin
    {
        public new ElderGuardPluginInformation GetPluginInformation()
        {
            return new ElderGuardPluginInformation()
            {
                Name = "System Monitor",
                Authors = new List<IElderGuardPluginInformationAuthor>()
                {
                    new ElderGuardPluginInformationAuthor()
                    {
                        GivenName = "Jordan",
                        EmailAddress = "test@test.com"
                    }
                }
            };
        }
    }
}
