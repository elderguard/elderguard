﻿using System.Runtime.Versioning;
using Autofac;
using ElderGuard.Container;
using ElderGuard.Concretes;
using ElderGuard.Services;
using ElderGuard.Plugins;
using ElderGuard.Core.PluginInfrastructure;
using ElderGuard.Concretes.PluginInfrastructure;
using ElderGuard.Core;
using Avalonia;
using Avalonia.Controls;
using ElderGuard.PluginInterOp.Interfaces;
using System.Collections.Generic;
using System.Linq;
using ElderGuard.Interfaces;
using Newtonsoft.Json.Linq;

namespace ElderGuard
{
    /// <summary>
    /// The main app instance.
    /// </summary>
    public class ElderGuardApp
    {
        /// <summary>
        /// The container instance.
        /// </summary>
        protected ElderGuardContainer container { get; set; }

        /// <summary>
        /// The instance of the app built using avalonia.
        /// </summary>
        protected AppBuilder BuiltUIInstance;

        /// <summary>
        /// The parameters received when launching the app.
        /// </summary>
        protected string[] incomingParams;
        
        /// <summary>
        /// The system tray.
        /// </summary>
        protected ElderGuardSystemTray? systemTray { get; set; }

        /// <summary>
        /// The interoperability layer for plugins.
        /// </summary>
        protected ElderGuardPluginInterOp? pluginInterOp { get; set; }

        /// <summary>
        /// Settings for the entire app loaded from the settings JSON file.
        /// </summary>
        protected ElderGuardAppSettings? settings { get; set; }

        /// <summary>
        /// The loaded plugins that we have in this 'ere app.
        /// </summary>
        protected List<IElderGuardPlugin> plugins = new List<IElderGuardPlugin>();

        /// <summary>
        /// Prepares the plugins list.
        /// </summary>
        protected void PrepPluginList()
        {
            using(var scope = container.useContainer().BeginLifetimeScope())
            {
                ElderGuardChromeAntiScamPlugin chromeAntiScam = scope.Resolve<ElderGuardChromeAntiScamPlugin>();
                ElderGuardConsoleAntiScamPlugin consoleAntiScam = scope.Resolve<ElderGuardConsoleAntiScamPlugin>();
                plugins.Add(chromeAntiScam);
                plugins.Add(consoleAntiScam);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void LoadPlugins()
        {
            ElderGuardConfigurationPluginConfigItem[]? config = (settings?.getConfiguration()?.PluginConfigurationItems);

            // If there's no settings, we can't load anything, and it's failed and I HATE YOU ALL!
            if (settings == null || config == null)
            {
                // TODO: Force reload and retry to be sure?
                // Note: Should always have a fallback, even in memory, and with highest protection.
                return;
            }

            foreach(IElderGuardPlugin plugin in plugins)
            {

                // Find the item in the settings for the plugin
                IEnumerable<ElderGuardConfigurationPluginConfigItem>? foundItems = (settings?.getConfiguration()?
                    .PluginConfigurationItems?
                    .Where(pluggy => plugin.GetPluginInformation().PluginName == pluggy.pluginName));

                // The items didn't exist, or there were none found.
                if(foundItems == null || foundItems != null && foundItems.Count() == 0)
                {
                    return;
                }

                ElderGuardConfigurationPluginConfigItem? item = foundItems?.First();

                if(item == null)
                {
                    return;
                }

                JObject? pluginSettings = item.pluginSettings as JObject;

                if(pluginSettings == null)
                {
                    return;
                }

                plugin.LoadPluginConfiguration(pluginSettings.First);
            }
        }

        /// <summary>
        /// Runs the main app for the Elderguard instance.
        /// </summary>
        protected void RunElderGuardApp()
        {
            // Prepares the plugin list.
            PrepPluginList();

            using (var scope = container.useContainer().BeginLifetimeScope())
            {
                // Initialise the systray immediately.
                systemTray = scope.Resolve<ElderGuardSystemTray>();

                // Gets the settings.
                settings = scope.Resolve<ElderGuardAppSettings>();

                // Loop all and inject object params.
                LoadPlugins();


                //var isTeamViewerInstalled = scope.Resolve<ElderGuardSoftwareManager>().refreshInstalledSoftware().isSoftwareInstalled("TeamViewer");
                //var chromey = scope.Resolve<ElderGuardChromeAntiScamPlugin>();
                //chromey.loadConfiguration(
                //    new ElderGuardPluginConfigItem(
                //        "ElderGuardChromeAntiScamPlugin",
                //        new { ShouldDisableDevTools = true }
                //    )
                //);

                //var commandy = scope.Resolve<ElderGuardConsoleAntiScamPlugin>();
                //commandy.loadConfiguration(new ElderGuardPluginConfigItem("ElderGuardCommandAntiScamPlugin", new { ShouldForceCloseCMD = true }));
            }
        }

        /// <summary>
        /// The application does its own stuff.
        /// </summary>
        public ElderGuardApp(AppBuilder appBuilder, string[] args)
        {
            BuiltUIInstance = appBuilder;
            incomingParams = args;
            container = new ElderGuardContainer(BuiltUIInstance);
            RunElderGuardApp();
        }
    }
}
