﻿using ElderGuard.Services;
using ElderGuard.Services.Common;
using ElderGuard.Concretes.Services;

namespace ElderGuard.Core.PluginInfrastructure
{
    /// <summary>
    /// This is established as a half-way house until DLL loading has been added in.
    /// </summary>
    public class ElderGuardPluginInterOp: ElderGuardService
    {
        /// <summary>
        /// The watcher
        /// </summary>
        protected ElderGuardFileWatcherService fileWatcherInstance;

        /// <summary>
        /// The registry instance.
        /// </summary>
        protected ElderGuardRegistryService registryServiceInstance;

        /// <summary>
        /// The software manager instance.
        /// </summary>
        protected ElderGuardSoftwareManager softwareManagerInstance;

        /// <summary>
        /// The instance of the process manager.
        /// </summary>
        protected ElderGuardProcessManager processManagerInstance;

        /// <summary>
        /// Uses a file watcher.
        /// NOTE: This is temporary.
        /// </summary>
        /// <returns></returns>
        public ElderGuardFileWatcherService useFileWatcher()
        {
            return this.fileWatcherInstance;
        }

        /// <summary>
        /// Uses the registry.
        /// NOTE: This is temporary.
        /// </summary>
        /// <returns></returns>
        public ElderGuardRegistryService useRegistry()
        {
            return this.registryServiceInstance;
        }

        /// <summary>
        /// Uses the software instance.
        /// NOTE: This is temporary.
        /// </summary>
        /// <returns></returns>
        public ElderGuardSoftwareManager useSoftwareManager()
        {
            return this.softwareManagerInstance;
        }

        /// <summary>
        //  Uses the process manager instance.
        /// NOTE: This is temporary.
        /// </summary>
        /// <returns></returns>
        public ElderGuardProcessManager useProcessManager()
        {
            return this.processManagerInstance;
        }

        /// <summary>
        /// A half-way house used for "fake" plugins to use until DLL loading is added.
        /// </summary>
        /// <param name="fileWatcher"></param>
        /// <param name="registryService"></param>
        /// <param name="softwareManager"></param>
        /// <param name=""></param>
        public ElderGuardPluginInterOp(
            ElderGuardFileWatcherService fileWatcher,
            ElderGuardRegistryService registryService,
            ElderGuardSoftwareManager softwareManager,
            ElderGuardProcessManager processManager,
            ElderGuardLogger logger
        ): base(logger)
        {
            fileWatcherInstance = fileWatcher;
            registryServiceInstance = registryService;
            softwareManagerInstance = softwareManager;
            processManagerInstance = processManager;
        }
    }
}
