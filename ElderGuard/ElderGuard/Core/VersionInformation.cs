﻿using ElderGuard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The core of it all.
/// </summary>
namespace ElderGuard.Core
{
    /// <summary>
    /// The version information for ElderGuard
    /// </summary>
    public class ElderGuardVersionInformation
    {
        /// <summary>
        /// The name of the assembly, basically.
        /// </summary>
        public string SoftwareName {
            get {
                string? name = Assembly.GetExecutingAssembly().GetName().Name;
                return name == null ? "" : name;
            } 
        }

        public string SoftwareSemanticVersion => String.Format("{0}.{1}.{2}", ThisAssembly.Git.SemVer.Major, ThisAssembly.Git.SemVer.Minor, ThisAssembly.Git.SemVer.Minor);
        public string SoftwareReleaseName => ThisAssembly.Git.Tag;
        public string SoftwareRevision => ThisAssembly.Git.Commit;
    }
}
