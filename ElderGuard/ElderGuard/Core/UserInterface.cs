﻿using Avalonia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// The UI.
/// </summary>
namespace ElderGuard.Core.UI
{
    /// <summary>
    /// The ElderGuard user UI.
    /// </summary>
    public class ElderGuardUserUI
    {
        /// <summary>
        /// The built UI.
        /// </summary>
        protected AppBuilder builtUI { get; set; }

        /// <summary>
        /// Displays the UI.
        /// </summary>
        public void DisplayUI()
        {
            return;
        }

        /// <summary>
        /// Takes the built UI and displays it when requested.
        /// </summary>
        /// <param name="builtAppInstance"></param>
        public ElderGuardUserUI(AppBuilder builtAppInstance)
        {
            builtUI = builtAppInstance;
        }
    }
}
