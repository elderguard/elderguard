﻿using ElderGuard.Concretes.Services;
using ElderGuard.Services;
using ElderGuard.Services.Common;
using ElderGuard.Exceptions;
using ElderGuard.Exceptions.Common;
using Serilog.Events;
using System;
using System.IO;
using System.Reflection;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ElderGuard.Concretes;

/// <summary>
/// The core namespace.
/// </summary>
namespace ElderGuard.Core
{
    /// <summary>
    /// The settings which elderguard uses to start.
    /// </summary>
    public class ElderGuardAppSettings
    {
        /// <summary>
        /// Default settings file name.
        /// </summary>
        protected static string SETTING_FILE_NAME = "ElderGuard.Settings.json";

        /// <summary>
        /// The default location for the path.
        /// </summary>
        protected static string DEFAULT_INSTALL_LOCATION = "C://Program Files//ElderGuard";

        /// <summary>
        /// The configuration which has been parsed from the config location.
        /// </summary>
        protected ElderGuardConfiguration? parsedConfiguration { get; set; }
        
        /// <summary>
        /// The instance that does the serialisation and deserialisation.
        /// </summary>
        protected JsonSerializer serializer { get; set; }

        /// <summary>
        /// Gets the configuration that has been parsed.
        /// </summary>
        /// <returns></returns>
        public ElderGuardConfiguration? getConfiguration()
        {
            return this.parsedConfiguration;
        }

        /// <summary>
        /// Gets the full path of the settings file.
        /// </summary>
        protected string FullSettingPath { 
            get 
            {
                // ASS MANNNNNN!
                string? assMan = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                if(assMan == null)
                {
                    return String.Format("{0}\\{1}", ElderGuardAppSettings.DEFAULT_INSTALL_LOCATION, ElderGuardAppSettings.SETTING_FILE_NAME);
                }

                return String.Format("{0}\\{1}", assMan, ElderGuardAppSettings.SETTING_FILE_NAME);
            } 
        }

        /// <summary>
        /// Checks to see if the settings file exists.
        /// </summary>
        protected async Task getSettingsFromFileSystem()
        {
            if (!File.Exists(this.FullSettingPath))
            {
                return;
            }

            CancellationToken token = new CancellationToken();

            // Get the contents of the file.
            try
            {
                // Get the file contents, then join them.
                string[] fileContents = await File.ReadAllLinesAsync(this.FullSettingPath, token);
                string contents = string.Join("", fileContents);


                // Now parse as YAML into an object and store it.
                parsedConfiguration = JsonConvert.DeserializeObject<ElderGuardConfiguration>(contents);
            }
            catch(FileNotFoundException)
            {
                throw new ElderGuardFileOrDirectoryNotFoundException();
            }
            catch(PathTooLongException)
            {
                throw new ElderGuardInsufficientPermissionsException();
            }
            catch (DirectoryNotFoundException)
            {
                throw new ElderGuardFileOrDirectoryNotFoundException();
            }
            catch (IOException)
            {
                throw new ElderGuardFileIOException();
            } 
            catch (UnauthorizedAccessException)
            {
                throw new ElderGuardInsufficientPermissionsException();
            } 
            catch (SecurityException)
            {
                throw new ElderGuardInsufficientPermissionsException();
            }
            catch (NotSupportedException)
            {
                throw new ElderGuardProgrammaticException();
            }

        }

        /// <summary>
        /// Separated function so we can call the setting load separately.
        /// </summary>
        protected void doLoadSettings()
        {
            // Gets the settings from the fs.
            this.getSettingsFromFileSystem()
                .Wait();
        }

        /// <summary>
        /// Initialises the settings.
        /// </summary>
        protected void init()
        {
            // Initialise the settings for error handling
            this.serializer.Error += HandleJSONParseErrors;

            // Loads settings from file.
            this.doLoadSettings();
        }

        /// <summary>
        /// Handles parsing of various errors which come from newtonsoft.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleJSONParseErrors(object? sender, Newtonsoft.Json.Serialization.ErrorEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialises the application's settings.
        /// </summary>
        /// <param name="fileWatcherService"></param>
        public ElderGuardAppSettings()
        {
            // Initialise the JSON serializer so we can get the settings.
            this.serializer = new JsonSerializer();
            this.init();
        }
    }
}
