﻿using ElderGuard.PluginInterOp.Interfaces.Services;
using ElderGuard.Services;

namespace ElderGuard.Core.InterOpExtensions
{
    public class ElderGuardSoftwareInterOpExtension: IElderGuardSoftwareInterOpExtension
    {
        /// <summary>
        /// The software manager instance.
        /// </summary>
        protected ElderGuardSoftwareManager softwareManagerInstance { get; set; }

        /// <summary>
        /// Refreshes the installed software.
        /// </summary>
        public void refreshInstalledSoftware()
        {
            this.softwareManagerInstance.refreshInstalledSoftware();
        }

        /// <summary>
        /// Checks if the given software is installed on the system.
        /// </summary>
        /// <param name="softwareName"></param>
        /// <returns></returns>
        public bool isSoftwareInstalled(string softwareName)
        {
            return this.softwareManagerInstance.isSoftwareInstalled(softwareName);
        }

        /// <summary>
        /// The extensions for using the 
        /// </summary>
        /// <param name="softwareManager"></param>
        public ElderGuardSoftwareInterOpExtension(
            ElderGuardSoftwareManager softwareManager
        )
        {
            softwareManagerInstance = softwareManager;
        }
    }
}
