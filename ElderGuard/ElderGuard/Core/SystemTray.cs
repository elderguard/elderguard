﻿using Avalonia.Controls;
using ElderGuard.Core.UI;
using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Forms;

namespace ElderGuard.Core
{
    /// <summary>
    /// Handles the system tray icons.
    /// </summary>
    public class ElderGuardSystemTray
    {
        /// <summary>
        /// The notification icon for the application.
        /// </summary>
        protected NotifyIcon AppNotifyIcon;

        /// <summary>
        /// The context menu for when the 
        /// </summary>
        protected ContextMenuStrip ContextMenu;

        /// <summary>
        /// The version information about this software.
        /// </summary>
        protected ElderGuardVersionInformation ElderGuardVersionInformation { get; set; }

        /// <summary>
        /// Gets the name of the tag (i.e. The release this refers to)
        /// </summary>
        protected string HeaderTagName
        {
            get
            {
                return this.ElderGuardVersionInformation.SoftwareReleaseName == "" ?
                    "Untagged-Release" : this.ElderGuardVersionInformation.SoftwareReleaseName;
            }
        }

        /// <summary>
        /// Generates the name for the header of the "about" component.
        /// </summary>
        protected string HeaderTextName {
            get {
                return String.Format("{0} \"{1}\" ({2}-{3})", this.ElderGuardVersionInformation.SoftwareName, this.HeaderTagName, this.ElderGuardVersionInformation.SoftwareSemanticVersion, this.ElderGuardVersionInformation.SoftwareRevision);
            }
        }

        /// <summary>
        /// Adds a basic context menu for the NotifyIcon.
        /// </summary>
        protected void initContextMenu()
        {
            ToolStripMenuItem headerItem = new ToolStripMenuItem();
            headerItem.Text = this.HeaderTextName;
            headerItem.Enabled = false;
            ContextMenu.Items.Add(headerItem);

            ToolStripSeparator separatorItem = new ToolStripSeparator();

            ToolStripMenuItem item = new ToolStripMenuItem();
            item.Text = "About";
            item.Enabled = true;
            ContextMenu.Items.Add(separatorItem);
            ContextMenu.Items.Add(item);
        }

        /// <summary>
        /// Initialises the notification icon.
        /// </summary>
        protected void initNotifyIcon()
        {
            // can of course use your own custom icon too.
            AppNotifyIcon = new NotifyIcon();
            AppNotifyIcon.Text = Assembly.GetExecutingAssembly().GetName().Name;
            AppNotifyIcon.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            // Add menu to tray icon and show it.
            AppNotifyIcon.ContextMenuStrip = this.ContextMenu;
            AppNotifyIcon.Visible = true;
        }

        /// <summary>
        /// Initialises the system tray.
        /// </summary>
        protected void init()
        {
            this.initContextMenu();
            this.initNotifyIcon();
        }

        /// <summary>
        /// A core provider which manages the system tray icon.
        /// </summary>
        public ElderGuardSystemTray(ElderGuardVersionInformation elderGuardVersionInformation)
        {
            this.ElderGuardVersionInformation = elderGuardVersionInformation;
            this.AppNotifyIcon = new NotifyIcon();
            this.ContextMenu = new ContextMenuStrip();
            this.init();
        }

    }
}
