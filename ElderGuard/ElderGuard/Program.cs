using Avalonia;
using Avalonia.Controls;
using System.Threading;

namespace ElderGuard
{
    class Program
    {
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        public static void Main(string[] args)
        {
            AppBuilder builtAppInstance = BuildAvaloniaApp();
            var elderGuardApp = new ElderGuardApp(builtAppInstance, args);
            builtAppInstance.Start(AppMain, args);
        }

        /// <summary>
        /// Manual configuration of the avalonia app launch so we can hide the main window at launch.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="args"></param>
        static void AppMain(Application app, string[] args)
        {
            // A cancellation token source that will be used to stop the main loop
            var cts = new CancellationTokenSource();

            // Start the main loop
            app.Run(cts.Token);
        }


        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToTrace();
    }
}
