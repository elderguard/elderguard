﻿using System;

namespace ElderGuard.Exceptions
{
    public class ElderGuardFileOrDirectoryNotFoundException: Exception
    {
    }

    public class ElderGuardFileIOException: Exception
    {

    }
}
