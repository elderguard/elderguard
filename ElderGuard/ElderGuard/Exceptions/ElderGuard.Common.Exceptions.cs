﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Exceptions.Common
{
    /// <summary>
    /// An exception which is thrown by this program.
    /// </summary>
    public class ElderGuardException : Exception {
        /// <summary>
        /// Is this an error we know is caused by wrong-implementation of this API?
        /// </summary>
        public bool IsProgrammaticException { get; set; }
    }
    
    /// <summary>
    /// An exception which 
    /// </summary>
    public class ElderGuardProgrammaticException: ElderGuardException
    {
        public ElderGuardProgrammaticException()
        {
            IsProgrammaticException = true;
        }
    }

    /// <summary>
    /// There was a problem, you don't have sufficient permissions.
    /// </summary>
    public class ElderGuardInsufficientPermissionsException : ElderGuardException
    {
    }

    /// <summary>
    /// The format of an input provided is wrong.
    /// </summary>
    public class ElderGuardInvalidFormatException: ElderGuardException
    {

    }
}
