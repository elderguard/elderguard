﻿using ElderGuard.Exceptions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Exceptions
{
    /// <summary>
    /// Hey there sexy linux(?) user, wanna kill all humans?
    /// </summary>
    public class ElderGuardUnsupportedOperatingSystemException : ElderGuardException
    {
    }

    /// <summary>
    /// The AV is going on a banana bonanza, thinking we're malware.
    /// </summary>
    public class ElderGuardAntiVirusException : ElderGuardException
    {

    }
}
