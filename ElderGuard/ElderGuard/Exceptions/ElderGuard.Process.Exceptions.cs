﻿using ElderGuard.Exceptions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Exceptions
{
    /// <summary>
    /// The process was not found.
    /// </summary>
   public class ElderGuardProcessNotFoundException: ElderGuardException
   {

   }
}
