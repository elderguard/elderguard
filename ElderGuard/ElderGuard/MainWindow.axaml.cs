using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System;
using System.Drawing;
using System.Reflection;
using System.Resources.Extensions;
using System.Windows.Forms;

namespace ElderGuard
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            // We do not want to display this on load.
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void OnExit(object sender, EventArgs e)
        {

        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
