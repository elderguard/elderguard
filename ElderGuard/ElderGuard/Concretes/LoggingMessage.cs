﻿using ElderGuard.Interfaces.Logging;
using Serilog.Events;

namespace ElderGuard.Concretes
{

    public class LoggingMessage<TExtraType>: ILoggingMessage<TExtraType>
    {
        public LogEventLevel level { get; set; }
        public string logMessage { get; set; }
        public TExtraType context { get; set; }

        public LoggingMessage(LogEventLevel logLevel, string message, TExtraType extra)
        {
            level = logLevel;
            logMessage = message;
            context = extra;
        }
    }

    public class LoggingMessage : ILoggingMessage
    {
        public LogEventLevel level { get; set; }
        public string logMessage { get; set; }

        public LoggingMessage(LogEventLevel logLevel, string message)
        {
            level = logLevel;
            logMessage = message;
        }
    }
}
