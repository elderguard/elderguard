﻿using ElderGuard.Services.Common;
using ElderGuard.Interfaces.Logging;
using Serilog.Events;
using ElderGuard.Core;

/// <summary>
/// All services for ElderGuard
/// </summary>
namespace ElderGuard.Concretes.Services
{
    /// <summary>
    /// Any class that can log items.
    /// </summary>
    public abstract class ElderGuardLoggable
    {
        /// <summary>
        /// The logger instance.
        /// </summary>
        protected ElderGuardLogger loggerInstance { get; set; }

        /// <summary>
        /// Logs a message.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public void Log(LogEventLevel level, string message)
        {
            this.loggerInstance.LogMessage(new LoggingMessage(level, message));
        }

        /// <summary>
        /// Logs the 
        /// </summary>
        /// <typeparam name="TLogContextType"></typeparam>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        public void Log<TLogContextType>(LogEventLevel level, string message, TLogContextType extra)
        {
            this.loggerInstance.LogMessage<TLogContextType>(new LoggingMessage<TLogContextType>(level, message, extra));
        }
    
        /// <summary>
        /// A default logger class.
        /// </summary>
        /// <param name="logger"></param>
        public ElderGuardLoggable (ElderGuardLogger logger)
        {
            this.loggerInstance = logger;
        }
    }

    /// <summary>
    /// A service which ElderGuard can call upon in a time of need.
    /// </summary>
    public abstract class ElderGuardService: ElderGuardLoggable
    {

        /// <summary>
        /// The service
        /// </summary>
        /// <param name="logger"></param>
        public ElderGuardService(ElderGuardLogger logger): base (logger)
        {
            
        }
    }
}
