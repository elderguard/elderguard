﻿using ElderGuard.Enums;
using ElderGuard.Interfaces.Services;

namespace ElderGuard.Concretes
{
    /// <summary>
    /// A concrete version of a process manager event.
    /// </summary>
    class ElderGuardProcessManagerEvent : IElderGuardProcessManagerEvent
    {
        public ElderGuardProcessManagerEventType? EventType { get; set; }
        public object? Context { get; set; }
    }
}
