﻿using System.Collections.Generic;

namespace ElderGuard.Concretes
{
    public class ElderguardWatcherConfigurationPathItem
    {
        public string? pathAsString { get; set; }
        public string? glob { get; set; }
    }

    public class ElderGuardWatcherConfiguration
    {
        /// <summary>
        /// The paths that the file watcher service should watch.
        /// </summary>
        public List<ElderguardWatcherConfigurationPathItem>? PathsToWatch { get; set; }
    }
}
