﻿using ElderGuard.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Concretes
{
    class ElderGuardianFileWatcherEvent : IElderGuardianFileWatcherEvent
    {
        public FileSystemEventArgs? args { get; set; }
    }
}
