﻿using ElderGuard.PluginInterOp.Interfaces;
using ElderGuard.Core.PluginInfrastructure;
using ElderGuard.Interfaces;
using ElderGuard.PluginInterOp.Enums;
using ElderGuard.PluginInterOp;

namespace ElderGuard.Concretes.PluginInfrastructure
{
    public abstract class ElderGuardPluginHost<TConfigurationType> : IElderGuardPlugin<TConfigurationType>
    {
        /// <summary>
        /// The plugin host for this plugin.
        /// </summary>
        protected ElderGuardPluginInterOp pluginInterop;

        public abstract IElderGuardPluginInformation GetPluginInformation();
        public abstract ElderGuardPluginState GetPluginState();
        public abstract void LoadPluginConfiguration(TConfigurationType pluginConfig);
        public abstract void  LoadPluginConfiguration(object pluginConfig);

        public ElderGuardPluginHost(
            ElderGuardPluginInterOp pluginInteropHost
        )
        {
            pluginInterop = pluginInteropHost;
        }
    }

    /// <summary>
    /// The configuration item concrete.
    /// </summary>
    public class ElderGuardPluginConfigItem : IElderGuardConfigurationPluginConfigItem
    {
        public string? pluginName { get; set; }
        public object? pluginSettings { get; set; }

        /// <summary>
        /// A configuration item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="settings"></param>
        public ElderGuardPluginConfigItem(string name, object settings)
        {
            pluginName = name;
            pluginSettings = settings;
        }
    }
}
