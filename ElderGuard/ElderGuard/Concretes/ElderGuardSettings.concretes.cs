﻿using ElderGuard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Concretes
{
    /// <summary>
    /// An individual plugin configuration item.
    /// </summary>
    public class ElderGuardConfigurationPluginConfigItem
    {
        public string? pluginName { get; set; }
        public object? pluginSettings { get; set; }
    }

    /// <summary>
    /// Configuration relating to logging.
    /// </summary>
    public class ElderGuardLoggingConfiguration
    {
        /// <summary>
        /// Is the logging itself enabled?
        /// </summary>
        public bool? enabled { get; set; }

        public string? logDirName { get; set; }
    }

    /// <summary>
    /// The configuration for elderGuard.
    /// </summary>
    public class ElderGuardConfiguration
    {
        public string? BaseDir { get; set; }
        public ElderGuardLoggingConfiguration? Logging { get; set; }
        public ElderGuardConfigurationPluginConfigItem[]? PluginConfigurationItems { get; set; }
    }
}
