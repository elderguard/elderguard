﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Helpers
{
    public class ElderGuardSystemPathHelper
    {

        /// <summary>
        /// Gets the locations for the program files.
        /// JJ/TODO: Older versions of windows if required.
        /// </summary>
        /// <returns></returns>
        public string[] getProgramFilesLocations()
        {
            return new string[] {
                Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86),
            };
        }
    }
}
