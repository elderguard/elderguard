﻿using Microsoft.Win32;
using ElderGuard.Services.Common;
using ElderGuard.Concretes.Services;
using ElderGuard.Exceptions.Common;
using System;
using Serilog.Events;
using System.Security;
using ElderGuard.Exceptions;
using System.IO;

namespace ElderGuard.Services
{
    public enum ElderGuardRegistryKeyLocation
    {
        User,
        sMachine,
    }

    /// <summary>
    /// 
    /// </summary>
    public class ElderGuardRegistryService : ElderGuardService
    {
        /// <summary>
        /// Adds a key to the given registry path.
        /// </summary>
        /// <param name="SubKeyLocation"></param>
        /// <param name="location"></param>
        public bool AddKeyToRegistryPath(
            string SubKeyLocation,
            ElderGuardRegistryKeyLocation location,
            string keyValueName, object value,
            RegistryValueKind valueKind
        )
        {
            RegistryKey foundKey = this.OpenOrCreateRegistryKey(SubKeyLocation, location);
            this.CreateRegistryKeyValue(foundKey, keyValueName, value, valueKind);
            return true;
        }

        /// <summary>
        /// Creates a key value in a registry subKey.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="keyValueName"></param>
        /// <param name="value"></param>
        /// <param name="kind"></param>
        protected void CreateRegistryKeyValue(RegistryKey key, string keyValueName, object value, RegistryValueKind kind)
        {
            try
            {
                object? foundValue = key.GetValue(keyValueName);
                key.SetValue(keyValueName, value, kind);
                key.Close();
            }
            catch (UnauthorizedAccessException unauthorisedEx)
            {
                this.Log<UnauthorizedAccessException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", unauthorisedEx);
                throw new ElderGuardInsufficientPermissionsException();
            }
            catch (ObjectDisposedException objectDisposedEx)
            {
                this.Log<ObjectDisposedException>(LogEventLevel.Error, "Attempted to create registry key, but key was expired. Potential antivirus hook-related issue?", objectDisposedEx);
                throw new ElderGuardAntiVirusException();
            }
            catch (SecurityException securityEx)
            {
                this.Log<SecurityException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", securityEx);
                throw new ElderGuardInsufficientPermissionsException();
            }
            catch (ArgumentNullException argumentNullEx)
            {
                this.Log<ArgumentNullException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", argumentNullEx);
                throw new ElderGuardProgrammaticException();
            }
            catch (IOException badCallEx)
            {
                this.Log<IOException>(LogEventLevel.Fatal, "Some programmatic error! You've tried to delete the root node of the registry!", badCallEx);
                throw new ElderGuardProgrammaticException();
            }
        }

        /// <summary>
        /// Opens or creates a registry key.
        /// </summary>
        /// <param name="SubKeyLocation"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        protected RegistryKey OpenOrCreateRegistryKey(string SubKeyLocation, ElderGuardRegistryKeyLocation location)
        {
            RegistryKey? key = this.OpenRegistryKey(SubKeyLocation, location);

            if(key != null)
            {
                return key;
            }

            try
            {
                if(location == ElderGuardRegistryKeyLocation.User)
                {
                    key = Registry.CurrentUser.CreateSubKey(SubKeyLocation, true);
                }
                else
                {
                    key = Registry.LocalMachine.CreateSubKey(SubKeyLocation, true);
                }
                return key;
            }
            catch (UnauthorizedAccessException unauthorisedEx)
            {
                this.Log<UnauthorizedAccessException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", unauthorisedEx);
                throw new ElderGuardInsufficientPermissionsException();
            }
            catch (ObjectDisposedException objectDisposedEx)
            {
                this.Log<ObjectDisposedException>(LogEventLevel.Error, "Attempted to create registry key, but key was expired. Potential antivirus hook-related issue?", objectDisposedEx);
                throw new ElderGuardAntiVirusException();
            }
            catch (SecurityException securityEx)
            {
                this.Log<SecurityException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", securityEx);
                throw new ElderGuardInsufficientPermissionsException();
            }
            catch (ArgumentNullException argumentNullEx)
            {
                this.Log<ArgumentNullException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", argumentNullEx);
                throw new ElderGuardProgrammaticException();
            }
        }

        /// <summary>
        /// Opens a registry key from the windows registry.
        /// </summary>
        /// <param name="SubKeyLocation"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        protected RegistryKey? OpenRegistryKey(string SubKeyLocation, ElderGuardRegistryKeyLocation location)
        {
            RegistryKey? foundKey;

            try
            {
                if (location == ElderGuardRegistryKeyLocation.User)
                {
                    foundKey = Registry.CurrentUser.OpenSubKey(SubKeyLocation, true);
                }
                else
                {
                    foundKey = Registry.LocalMachine.OpenSubKey(SubKeyLocation, true);
                }

                return foundKey;
            } 
            catch(ObjectDisposedException objectDisposedEx)
            {
                this.Log<ObjectDisposedException>(LogEventLevel.Error, "Attempted to create registry key, but key was expired. Potential antivirus hook-related issue?", objectDisposedEx);
                throw new ElderGuardAntiVirusException();
            }
            catch(SecurityException securityEx)
            {
                this.Log<SecurityException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", securityEx);
                throw new ElderGuardInsufficientPermissionsException();
            }
            catch(ArgumentNullException argumentNullEx)
            {
                this.Log<ArgumentNullException>(LogEventLevel.Fatal, "Not running with sufficient permissions or registry key deleted/sandboxed.", argumentNullEx);
                throw new ElderGuardProgrammaticException();
            }
     
        }


        /// <summary>
        /// Creates a new instance of the ElderGuardRegistryService.
        /// </summary>
        /// <param name="logger"></param>
        public ElderGuardRegistryService(ElderGuardLogger logger): base(logger)
        {

        }
    }
}
