﻿using ElderGuard.Concretes.Services;
using ElderGuard.Concretes;
using ElderGuard.Exceptions;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System;
using Serilog.Events;
using ElderGuard.Exceptions.Common;
using System.ComponentModel;
using ElderGuard.Services.Common;
using System.Management;
using ElderGuard.Interfaces.Services;

namespace ElderGuard.Services
{
    /// <summary>
    /// Manager that looks after running processes on Windows.
    /// </summary>
    public class ElderGuardProcessManager : ElderGuardService
    {
        /// <summary>
        /// Query for when processes are started.
        /// </summary>
        protected static string RUNNING_PROCESSES_STARTED_QUERY = "SELECT * FROM Win32_ProcessStartTrace";

        /// <summary>
        /// The MVE that refers to running processes.
        /// </summary>
        protected ManagementEventWatcher? processWatcher;

        /// <summary>
        /// Processes to listen for.
        /// </summary>
        protected List<string> listenForProcesses = new List<string>();

        /// <summary>
        /// Kills the processes specified as soon as they open.
        /// </summary>
        protected List<string> killProcesses = new List<string>();

        /// <summary>
        /// Does this service have anything to listen to?
        /// </summary>
        protected bool hasProcessesToListenTo { get => listenForProcesses.Count != 0 || this.killProcesses.Count != 0; }

        /// <summary>
        /// Changes that occur when processes are loaded.
        /// </summary>
        public event EventHandler<IElderGuardProcessManagerEvent>? onProcessChange;

        /// <summary>
        /// Kills a process with a given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool KillProcessByName(string name)
        {
            // Get the processes out of the 
            try
            {
                string nameWithoutExecutable = name
                    .Replace(".exe", "")
                    .Replace(".dll", "")
                    .Replace(".msi", "");

                List<Process> processes = Process.GetProcesses()
                        .Where(p => p.ProcessName.Equals(nameWithoutExecutable))
                        .ToList<Process>();

                if(processes.Count == 0)
                {
                    return false;
                }

                processes.ForEach(p => p.Kill());
                return true;
            } 
            catch(InvalidOperationException invalidOperationEx)
            {
                this.Log<InvalidOperationException>(LogEventLevel.Error, "Received invalid operation; likely no permissions.", invalidOperationEx);
                throw new ElderGuardInsufficientPermissionsException();
            } 
            catch (ArgumentNullException argumentNullException)
            {
                this.Log<ArgumentNullException>(LogEventLevel.Error, "Received argument null exception. This is likely a build error with this application.", argumentNullException);
                throw new ElderGuardProgrammaticException();
            }
            catch (ArgumentException argumentException)
            {
                this.Log<ArgumentException>(LogEventLevel.Error, "Received argument null exception. This is likely a build error with this application.", argumentException);
                throw new ElderGuardProgrammaticException();
            }
            catch (Win32Exception w32Exception)
            {
                this.Log<Win32Exception>(LogEventLevel.Error, "Receieved Win32 Exception", w32Exception);
                throw new ElderGuardProgrammaticException();
            }
            catch (PlatformNotSupportedException youSexyLinuxBastard)
            {
                this.Log<PlatformNotSupportedException>(LogEventLevel.Error, "You're running Linux, this app isn't meant to run on Linux. Wanna port it for a laugh? :D", youSexyLinuxBastard);
                throw new ElderGuardUnsupportedOperatingSystemException();
            }
        }

        /// <summary>
        /// Watches for a process with a given name.
        /// </summary>
        /// <param name="processName"></param>
        public void WatchForProcessesByName(List<string> processNames)
        {
            this.listenForProcesses = this.listenForProcesses.Concat(processNames)
                .ToList<string>();
        }

        public void AutoKillProcessesByName(List<string> processNames)
        {
            this.killProcesses = this.killProcesses.Concat(processNames)
                .ToList<string>();
        }

        /// <summary>
        /// Starts the process watcher so we can tell when new processes are started.
        /// </summary>
        protected void startProcessWatcher()
        {
            this.processWatcher = new ManagementEventWatcher(new WqlEventQuery(ElderGuardProcessManager.RUNNING_PROCESSES_STARTED_QUERY));
            this.processWatcher.EventArrived += ProcessWatcher_EventArrived;
            this.processWatcher.Start();
        }

        /// <summary>
        /// Handles when new events come from processes started.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProcessWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            if(!this.hasProcessesToListenTo)
            {
                return;
            }

            string? ProcessName = (string) e.NewEvent.Properties["ProcessName"].Value;

            if (this.killProcesses.Contains(ProcessName))
            {
                this.KillProcessByName(ProcessName);
                return;
            }

            if (!this.listenForProcesses.Contains(ProcessName))
            {
                this.Log(LogEventLevel.Verbose, "Received process listen but isn't anything we care about");
                return;
            }



            this.onProcessChange?.Invoke(
                this,
                new ElderGuardProcessManagerEvent() { Context = new { ProcessName = e.NewEvent.Properties["ProcessName"].ToString() }, EventType = Enums.ElderGuardProcessManagerEventType.ProcessStarted }
            );
        }

        /// <summary>
        /// Initialises the Process Manager processes.
        /// </summary>
        protected void init()
        {
            this.startProcessWatcher();
        }

        /// <summary>
        /// Creates a new instance of the process manager.
        /// </summary>
        /// <param name="logger"></param>
        public ElderGuardProcessManager(ElderGuardLogger logger) : base(logger)
        {
            this.init();
        }

    }
}
