﻿using Microsoft.Win32;
using System.Security;
using ElderGuard.Exceptions;
using ElderGuard.Services.Common;
using ElderGuard.Concretes.Services;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Linq;
using ElderGuard.Helpers;
using Serilog.Events;
using ElderGuard.Exceptions.Common;

namespace ElderGuard.Services
{
    public enum RegistryInstalledLocation
    {
        CurrentUser,
        LocalMachine
    }

    /// <summary>
    /// The software manager looks after what software is stored.
    /// </summary>
    public class ElderGuardSoftwareManager: ElderGuardService
    {
        /// <summary>
        /// The registry key as a string for looking at installed software.
        /// </summary>
        protected static string[] INSTALLED_KEYS_CURRENTUSER = new string[] {
            "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
        };

        protected static string[] INSTALLED_KEYS_LOCALMACHINE = new string[] {
            "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
            "SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
        };

        /// <summary>
        /// A list of the software installed by location.
        /// </summary>
        protected Dictionary<string, List<string>> foundInstalledSoftware = new Dictionary<string, List<string>>();

        /// <summary>
        /// A list of all software installed.
        /// </summary>
        protected IEnumerable<string> allInstalledSoftware = new List<string>();

        /// <summary>
        /// The file watcher service instance.
        /// </summary>
        protected ElderGuardFileWatcherService FileWatcherService;

        /// <summary>
        /// The path helper.
        /// </summary>
        protected ElderGuardSystemPathHelper PathHelper;

        /// <summary>
        /// Checks to see if software is installed.
        /// </summary>
        /// <param name="softwareItem"></param>
        /// <returns></returns>
        public bool isSoftwareInstalled(string softwareItem)
        {
            return this.allInstalledSoftware.ToList().Find(e => e.Contains(softwareItem)) != null;
        }

        /// <summary>
        /// Refreshes the installed software.
        /// </summary>
        [SupportedOSPlatform("windows")]
        public ElderGuardSoftwareManager refreshInstalledSoftware()
        {
            this.foundInstalledSoftware.Clear();
            this.LoadSoftwareInstalled();
            return this;
        }

        /// <summary>
        /// Loads the installed software from the system.
        /// </summary>
        [SupportedOSPlatform("windows")]
        protected void LoadSoftwareInstalled()
        {
            this.Log(LogEventLevel.Verbose, string.Format("Found {0} items to check in HKEY_CURRENT_USER", ElderGuardSoftwareManager.INSTALLED_KEYS_CURRENTUSER));
            foreach(string key in ElderGuardSoftwareManager.INSTALLED_KEYS_CURRENTUSER)
            {
                this.Log(LogEventLevel.Verbose, string.Format("Looking for software installed in {0}", key));
                List<string> keysFound = EnumerateInstalledSoftware(key, RegistryInstalledLocation.CurrentUser);
                this.Log(LogEventLevel.Verbose, string.Format("Found {0} software items installed in {1}", keysFound.Count, key));
                addToInstalledSoftware(RegistryInstalledLocation.CurrentUser, key, keysFound);
                allInstalledSoftware = allInstalledSoftware.Concat(keysFound);
            }

            this.Log(LogEventLevel.Verbose, string.Format("Found {0} items to check in HKEY_LOCAL_MACHINE", ElderGuardSoftwareManager.INSTALLED_KEYS_CURRENTUSER));
            foreach (string key in ElderGuardSoftwareManager.INSTALLED_KEYS_LOCALMACHINE)
            {
                this.Log(LogEventLevel.Verbose, string.Format("Looking for software installed in {0}", key));
                List<string> keysFound = EnumerateInstalledSoftware(key, RegistryInstalledLocation.LocalMachine);
                this.Log(LogEventLevel.Verbose, string.Format("Found {0} software items installed in {1}", keysFound.Count, key));
                addToInstalledSoftware(RegistryInstalledLocation.LocalMachine, key, keysFound);
                allInstalledSoftware = allInstalledSoftware.Concat(keysFound);
            }

            this.Log(LogEventLevel.Verbose, "Completed outlining software");
        }

        protected void addToInstalledSoftware(RegistryInstalledLocation location, string key, List<string> keysFound)
        {
            if(foundInstalledSoftware.ContainsKey(location.ToString() + key))
            {
                return;
            }

            foundInstalledSoftware.Add(location.ToString() + key, keysFound);
        }

        [SupportedOSPlatform("windows")]
        protected List<string> EnumerateInstalledSoftware(string key, RegistryInstalledLocation location)
        {
            try
            {

                RegistryKey? useKey;

                if (location == RegistryInstalledLocation.LocalMachine)
                {
                    useKey = Registry.LocalMachine.OpenSubKey(key);
                } else
                {
                    useKey = Registry.CurrentUser.OpenSubKey(key);
                }

                /// Your windows is seriously problematic if this happens.
                if(useKey == null)
                {
                    return new List<string>();
                }

                List<string> keysFound = GetKeysInLocation(useKey);
                return keysFound;
            } catch(SecurityException)
            {
                throw new ElderGuardInsufficientPermissionsException();
            }
        }

        /// <summary>
        /// For a given location, gets the keys.
        /// </summary>
        /// <param name="useKey"></param>
        [SupportedOSPlatform("windows")]
        protected List<string> GetKeysInLocation(RegistryKey useKey)
        {
            List<string> values = new List<string>();

            foreach (string keyName in useKey.GetSubKeyNames())
            {
                RegistryKey? subKey = useKey.OpenSubKey(keyName);

                if (subKey != null)
                {
                    object? keyValue = subKey.GetValue("DisplayName");

                    if (keyValue != null)
                    {
                        values.Add((string)keyValue);
                    }
                }
            }
            return values;
        }

        public ElderGuardSoftwareManager(
            ElderGuardFileWatcherService fileWatcherService,
            ElderGuardSystemPathHelper systemPathHelper,
            ElderGuardLogger logger
        ): base(logger)
        {
            this.FileWatcherService = fileWatcherService;
            this.PathHelper = systemPathHelper;
        }

    }
}
