﻿using Serilog;
using Serilog.Core;
using ElderGuard.Interfaces.Logging;
using ElderGuard.Core;
using System.IO;

namespace ElderGuard.Services.Common
{
    public class ElderGuardLogger
    {
        /// <summary>
        /// The default directory that elderguard sits in
        /// </summary>
        protected static string DEFAULT_BASE_DIR = "C:/Program Files/ElderGuard";

        /// <summary>
        /// The default directory name for the logs.
        /// </summary>
        protected static string DEFAULT_LOG_DIR_NAME = "Logs";

        /// <summary>
        /// The filename for the logs.
        /// </summary>
        protected static string LOG_FILE_NAME = "ElderGuard.log";

        /// <summary>
        /// The log file format.
        /// </summary>
        protected static string LOG_FILE_FORMAT = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}";

        /// <summary>
        /// The logger instance.
        /// </summary>
        protected Logger? LoggerInstance;

        /// <summary>
        /// The settings from the settings file.
        /// </summary>
        protected ElderGuardAppSettings appSettings;

        /// <summary>
        /// The base directory for the logs
        /// </summary>
        protected string BaseLogDir
        {
            get
            {
                string? baseDir = this.appSettings.getConfiguration()?.BaseDir;

                if(baseDir == null)
                {
                    return ElderGuardLogger.DEFAULT_BASE_DIR;
                }
                return baseDir;
            }
        }

        protected string LogDirName
        {
            get
            {
                string? logDirName = this.appSettings.getConfiguration()?.Logging?.logDirName;

                if(logDirName == null)
                {
                    return ElderGuardLogger.DEFAULT_LOG_DIR_NAME;
                }

                return logDirName;
            }
        }

        /// <summary>
        /// The full directory name for the logfile.
        /// </summary>
        protected string FullLogFileDirPath
        {
            get
            {
                return string.Format("{0}/{1}", BaseLogDir, LogDirName);
            }
        }

        /// <summary>
        /// Logs a message to the log.
        /// </summary>
        /// <param name="message"></param>
        public void LogMessage<TMessageType>(ILoggingMessage<TMessageType> message)
        {
            this.LoggerInstance?.Write<TMessageType>(message.level, message.logMessage, message.context);
        }

        /// <summary>
        /// Logs a message but without extra parameters.
        /// </summary>
        /// <param name="message"></param>
        public void LogMessage(ILoggingMessage message)
        {
            this.LoggerInstance?.Write(message.level, message.logMessage);
        }

        protected void init()
        {
            if(!Directory.Exists(this.FullLogFileDirPath))
            {
                Directory.CreateDirectory(this.FullLogFileDirPath);
            }

            this.LoggerInstance = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo
                    .File(
                        this.FullLogFileDirPath + ElderGuardLogger.LOG_FILE_NAME,
                        rollingInterval: RollingInterval.Day,
                        retainedFileCountLimit: null,
                        outputTemplate: ElderGuardLogger.LOG_FILE_FORMAT
                    )
                .CreateLogger();
        }

        /// <summary>
        /// Creates a new instance of the ElderGuardLogger
        /// </summary>
        public ElderGuardLogger(ElderGuardAppSettings settings)
        {
            appSettings = settings;
            init();
        }
    }
}
