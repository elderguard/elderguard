﻿
using ElderGuard.Interfaces.Services;
using ElderGuard.Concretes.Services;
using ElderGuard.Services.Common;
using System;
using System.Collections.Generic;
using System.IO;
using Serilog.Events;
using ElderGuard.Concretes;

namespace ElderGuard.Services
{
    /// <summary>
    /// A service which watches for filesystem changes.
    /// </summary>
    public class ElderGuardFileWatcherService: ElderGuardService
    {
        /// <summary>
        /// The event for changes in the file system as a singular event.
        /// </summary>
        public event EventHandler<IElderGuardianFileWatcherEvent>? onFileHandlerChange;

        /// <summary>
        /// Dictionary of watcher instances we are using
        /// </summary>
        protected Dictionary<string, FileSystemWatcher> pathWatchers = new Dictionary<string, FileSystemWatcher>();

        /// <summary>
        /// Initialises the watchers for a given path.
        /// </summary>
        /// <param name="path"></param>
        protected void initialiseWatcherForPath(ElderguardWatcherConfigurationPathItem path)
        {
            if(path.pathAsString == null || path.glob == null || this.pathWatchers.ContainsKey(path.pathAsString))
            {
                return;
            }

            // Register the watcher.
            FileSystemWatcher watcher = new FileSystemWatcher(path.pathAsString, path.glob)
            {
                NotifyFilter = NotifyFilters.Attributes
                                  | NotifyFilters.CreationTime
                                  | NotifyFilters.DirectoryName
                                  | NotifyFilters.FileName
                                  | NotifyFilters.LastAccess
                                  | NotifyFilters.LastWrite
                                  | NotifyFilters.Security
                                  | NotifyFilters.Size
            };

            watcher.Changed += WatcherEventFired;
            watcher.Created += WatcherEventFired;
            watcher.Deleted += WatcherEventFired;
            watcher.Renamed += WatcherEventFired;
            watcher.Error += WatcherErrorFired;
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;

            pathWatchers.Add(path.pathAsString, watcher);
        }

        /// <summary>
        /// Encountered an error when doing watching.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WatcherErrorFired(object sender, ErrorEventArgs e)
        {
            // TODO: Added logger
            this.Log<ErrorEventArgs>(LogEventLevel.Warning, "Attempted to watch path but there was an error.", e);
        }

        /// <summary>
        /// The watcher saw a file change.s
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WatcherEventFired(object sender, FileSystemEventArgs e)
        {
            this.Log<FileSystemEventArgs>(LogEventLevel.Verbose, "Received watcher event", e);
            this.sendEvent(e);
        }

        /// <summary>
        /// When an event was registered on one of the items, do something.
        /// </summary>
        /// <param name="args"></param>
        protected void sendEvent(FileSystemEventArgs args)
        {
            this.onFileHandlerChange?.Invoke(this, new ElderGuardianFileWatcherEvent() { args = args });
        }

        /// <summary>
        /// Initialises the paths for watchers.
        /// </summary>
        public void AddWatch(ElderGuardWatcherConfiguration configInstance)
        {
            /// We want to do nothing.
            if(configInstance.PathsToWatch == null || configInstance.PathsToWatch.Count == 0)
            {
                this.Log(LogEventLevel.Warning, "Attempted to add watches, but a consumer provided empty watch paths.");
                return;
            }

            // Loops each path, registers a watcher.
            configInstance.PathsToWatch.ForEach((ElderguardWatcherConfigurationPathItem path) => this.initialiseWatcherForPath(path));
        }

        /// <summary>
        /// Constructor which calls the base ElderGuardService class.
        /// </summary>
        /// <param name="logger"></param>
        public ElderGuardFileWatcherService(ElderGuardLogger logger): base(logger)
        {

        }

    }
}
