﻿using Autofac;
using ElderGuard.Helpers;
using ElderGuard.Services;
using ElderGuard.Services.Common;
using ElderGuard.Core.PluginInfrastructure;
using System.Runtime.Versioning;
using ElderGuard.Plugins;
using ElderGuard.Core;
using Avalonia;
using ElderGuard.Core.UI;
using Avalonia.Controls;

/// <summary>
/// Inversion of control container-related code.
/// </summary>
namespace ElderGuard.Container {
    /// <summary>
    /// The main container of the app.
    /// </summary>
    public class ElderGuardContainer
    {
        /// <summary>
        /// The AutoFac builder instance.
        /// </summary>
        protected ContainerBuilder builderInstance { get; set; }

        /// <summary>
        /// The container for the item.
        /// </summary>
        protected IContainer container { get; set; }

        /// <summary>
        /// Registers the container instances to be initialised.
        /// </summary>
        [SupportedOSPlatform("windows")]
        protected void RegisterContainerInstances(AppBuilder builtApp)
        {
            // Take the built app instance and insert it first, so we have it.
            this.builderInstance.RegisterInstance<AppBuilder>(builtApp)
                .SingleInstance();

            // Register the service managing the UI to display and hide at will.
            this.builderInstance.RegisterType<ElderGuardUserUI>()
                .SingleInstance();

            this.builderInstance.RegisterType<ElderGuardAppSettings>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardVersionInformation>()
                .SingleInstance();

            // The watcher service.
            this.builderInstance.RegisterType<ElderGuardLogger>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardSystemTray>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardFileWatcherService>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardSystemPathHelper>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardRegistryService>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardProcessManager>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardSoftwareManager>()
                .OnActivating(e => e.Instance.refreshInstalledSoftware())
                .SingleInstance();

            // PLUGIN INTEROP
            this.builderInstance.RegisterType<ElderGuardPluginInterOp>()
                .SingleInstance();

            // TEMPORARY PLUGIN LOADING HERE.
            this.builderInstance.RegisterType<ElderGuardChromeAntiScamPlugin>()
                .SingleInstance();
            this.builderInstance.RegisterType<ElderGuardConsoleAntiScamPlugin>()
                .SingleInstance();
        }

        /// <summary>
        /// Uses the container manually.
        /// </summary>
        /// <returns></returns>
        public IContainer useContainer() {
            return container;
        }

        /// <summary>
        /// Creates the instance of the container.
        /// </summary>
        [SupportedOSPlatform("windows")]
        public ElderGuardContainer(AppBuilder builtApp)
        {
            // Create AutoFAQ's container.
            builderInstance = new ContainerBuilder();

            // Add stuff into the container.
            RegisterContainerInstances(builtApp);

            // Builds the container.
            container = builderInstance.Build();
        }
    }
}
