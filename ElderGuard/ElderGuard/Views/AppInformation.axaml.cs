using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace ElderGuard.Views
{
    public partial class AppInformation : UserControl
    {
        public AppInformation()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
