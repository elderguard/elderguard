using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace ElderGuard.Views
{
    public partial class HomeDisplay : UserControl
    {
        public HomeDisplay()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
