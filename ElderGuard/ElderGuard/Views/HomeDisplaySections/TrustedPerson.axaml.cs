using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace ElderGuard.Views.HomeDisplaySections
{
    public partial class TrustedPerson : UserControl
    {
        public TrustedPerson()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
