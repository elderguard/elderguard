﻿using ElderGuard.Concretes.PluginInfrastructure;
using ElderGuard.Core.PluginInfrastructure;
using ElderGuard.Exceptions;
using ElderGuard.Exceptions.Common;
using ElderGuard.Interfaces;
using ElderGuard.PluginInterOp;
using ElderGuard.PluginInterOp.Concretes;
using ElderGuard.PluginInterOp.Enums;
using ElderGuard.PluginInterOp.Interfaces;
using Microsoft.Win32;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

/// <summary>
/// Plugins for ElderGuard.
/// </summary>
namespace ElderGuard.Plugins
{
    /// <summary>
    /// Configuration for the chrome anti-scam plugin.
    /// </summary>
    public interface IElderGuardChromeAntiScamPluginConfig
    {
        /// <summary>
        /// Should disable the developer tools.
        /// </summary>
        bool ShouldDisableDevTools { get; set; }
    }

    /// <summary>
    /// The configuration for the chrome-based anti-scam stuffs.
    /// </summary>
    public class ElderGuardChromeAntiScamPluginConfig: IElderGuardChromeAntiScamPluginConfig
    {
        public bool ShouldDisableDevTools { get; set; }
    }

    /// <summary>
    /// A plugin which handles anti-scamming in relation to Chrome.
    /// </summary>
    public class ElderGuardChromeAntiScamPlugin : ElderGuardPluginHost<ElderGuardChromeAntiScamPluginConfig>
    {
        /// <summary>
        /// The names of the software installed.
        /// </summary>
        protected static string[] CHROME_SOFTWARE_NAMES = new string[]
        {
            "Google Chrome",
            "Chromium"
        };

        /// <summary>
        /// The path to the Devtools disable setting.
        /// </summary>
        protected static string CHROME_CURRENT_USER_DISABLE_DEV_TOOLS_PATH = "Software\\Policies\\Google\\Chrome";

        /// <summary>
        /// The key which states that Developer tools are disabled (Currently is REG_DWORD, 1 = enabled, 0 = disabled)
        /// https://admx.help/?Category=Chrome&Policy=Google.Policies.Chrome::DeveloperToolsDisabled&Language=en-gb
        /// </summary>
        protected static string CHROME_DISABLE_DEV_TOOLS_KEY_NAME = "DeveloperToolsDisabled";

        /// <summary>
        /// The Chrome variants that are installed on this machine.
        /// </summary>
        protected Dictionary<string, bool> chromeVariantsInstalled = new Dictionary<string, bool>();

        /// <summary>
        /// The configuration for this plugin.
        /// </summary>
        protected IElderGuardChromeAntiScamPluginConfig? pluginConfig;

        /// <summary>
        /// Is at least one variant of Chrome installed?
        /// </summary>
        protected bool IsChromeInstalled { get; set; }

        /// <summary>
        /// The plugin information which defines this functionality.
        /// </summary>
        /// <returns></returns>
        public override IElderGuardPluginInformation GetPluginInformation()
        {
            return new ElderGuardPluginInformation()
            {
                DisplayName = "Chrome Anti-Scam",
                PluginName = "ChromeAntiScam",
                Authors = new List<IElderGuardPluginInformationAuthor>()
                {
                    new ElderGuardPluginInformationAuthor()
                    {
                        GivenName = "Jordàn C. (Grimorio)",
                        EmailAddress = "grimorio@riseup.net"
                    }
                },
            };
        }

        /// <summary>
        /// Lazy-load the configuration, when the plugin has orchestrated stuff.
        /// </summary>
        /// <param name="configItem"></param>
        public override void LoadPluginConfiguration(object configSettings)
        {
            if (configSettings == null)
            {
                this.pluginInterop.Log(LogEventLevel.Warning, "Config item passed to plugin was null");
                return;
            }

            PropertyInfo? disableDevTools = configSettings
                    .GetType()
                    .GetProperty("ShouldDisableDevTools");

            if(disableDevTools == null)
            {
                return;
            }

            object? disableDevToolsSettingValue = disableDevTools.GetValue(configSettings, null);

            if (disableDevToolsSettingValue == null || this.pluginConfig == null) 
            {
                return;
            }

            this.pluginConfig = new ElderGuardChromeAntiScamPluginConfig()
            {
                ShouldDisableDevTools = (bool)disableDevToolsSettingValue,
            };

            this.OnPluginConfigLoaded();
        }

        /// <summary>
        /// Checks to see if chrome is installed on the target machine.
        /// </summary>
        protected void CheckChromeIsInstalled()
        {
            // Refresh installed software.
            this.pluginInterop.useSoftwareManager()
                .refreshInstalledSoftware();

            this.chromeVariantsInstalled.Clear();

            // Loops each variant, then checks and sees if that is installed.
            foreach(string chromeVariantname in ElderGuardChromeAntiScamPlugin.CHROME_SOFTWARE_NAMES)
            {
                bool isInstalled = this.pluginInterop.useSoftwareManager().isSoftwareInstalled(chromeVariantname);
                this.chromeVariantsInstalled.Add(chromeVariantname, isInstalled);
            }

            // Let's log that something is installed, until we restart the process.
            this.IsChromeInstalled = this.chromeVariantsInstalled.Where(kvPair => kvPair.Value == true).Count() > 0;
        }

        protected void doSetDisableChromeDevToolsRegistryKV(bool disabled)
        {
            try { 
                this.pluginInterop.useRegistry()
                    .AddKeyToRegistryPath(
                        ElderGuardChromeAntiScamPlugin.CHROME_CURRENT_USER_DISABLE_DEV_TOOLS_PATH,
                        Services.ElderGuardRegistryKeyLocation.User,
                        ElderGuardChromeAntiScamPlugin.CHROME_DISABLE_DEV_TOOLS_KEY_NAME,
                        disabled ? 1 : 0,
                        RegistryValueKind.DWord
                    );
            }
            catch (ElderGuardInsufficientPermissionsException insufficientPermissionsEx)
            {
                this.pluginInterop.Log<ElderGuardInsufficientPermissionsException>(LogEventLevel.Error, "Can't create registry item because we don't have permissions", insufficientPermissionsEx);
            }
            catch (ElderGuardAntiVirusException antivirusEx)
            {
                this.pluginInterop.Log<ElderGuardAntiVirusException>(LogEventLevel.Error, "Can't create registry because potentially sandboxed.", antivirusEx);
            }
            catch (ElderGuardProgrammaticException programmaticEx)
            {
                this.pluginInterop.Log<ElderGuardProgrammaticException>(LogEventLevel.Fatal, "Programmatic error!", programmaticEx);
            }
        }

        /// <summary>
        /// Re-enables chrome dev tools.
        /// </summary>
        protected void doEnableChromeDevTools()
        {
            // Write to registry!
            this.doSetDisableChromeDevToolsRegistryKV(false);
        }

        /// <summary>
        /// Disables chrome dev tools.
        /// </summary>
        protected void doDisableChromeDevTools()
        {
            this.pluginInterop.Log(LogEventLevel.Verbose, "Starting operation -> Disable Chrome Dev Tools");

            // Write to registry!
            this.doSetDisableChromeDevToolsRegistryKV(true);
            this.pluginInterop.Log(LogEventLevel.Verbose, "Concluded operation -> Disable Chrome Dev Tools");
        }

        /// <summary>
        /// Disables chrome dev tools.
        /// </summary>
        protected void DisableChromeDevTools()
        {
            // Checks if chrome is installed first, if not no need.
            this.CheckChromeIsInstalled();

            // Are there any chrome variants installed?
            if(this.IsChromeInstalled)
            {
                this.doDisableChromeDevTools();
            }
        }

        /// <summary>
        /// Called when the configuration changes.
        /// </summary>
        protected void handleConfigChange()
        {
            if(this.pluginConfig != null && this.pluginConfig.ShouldDisableDevTools)
            {
                this.DisableChromeDevTools();
            }
        }

        protected void OnPluginConfigLoaded()
        {
            this.handleConfigChange();
        }

        /// <summary>
        /// Parses out the configuration for this plugin.
        /// </summary>
        protected void parsePluginConfiguration(object config)
        {
            // Gets the plugin config.
            this.pluginConfig = (IElderGuardChromeAntiScamPluginConfig)(ElderGuardChromeAntiScamPluginConfig)config;
            this.OnPluginConfigLoaded();
        }

        /// <summary>
        /// Initialises the plugin.
        /// </summary>
        protected void init()
        {
            // Check chrome is installed.
            this.CheckChromeIsInstalled();
        }

        public override ElderGuardPluginState GetPluginState()
        {
            throw new NotImplementedException();
        }

        public override void LoadPluginConfiguration(ElderGuardChromeAntiScamPluginConfig pluginConfig)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialises the anti-scam plugin.
        /// </summary>
        /// <param name="pluginInterOp"></param>
        public ElderGuardChromeAntiScamPlugin(
            ElderGuardPluginInterOp pluginInterOp
        ) : base(pluginInterOp) {
            this.init();
        }
    }
}
