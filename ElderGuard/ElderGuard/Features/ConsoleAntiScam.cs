﻿using ElderGuard.Concretes.PluginInfrastructure;
using ElderGuard.Core.PluginInfrastructure;
using ElderGuard.Exceptions;
using ElderGuard.Exceptions.Common;
using ElderGuard.Interfaces;
using ElderGuard.PluginInterOp.Concretes;
using ElderGuard.PluginInterOp.Enums;
using ElderGuard.PluginInterOp.Interfaces;
using Microsoft.Win32;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

/// <summary>
/// Plugins for ElderGuard.
/// </summary>
namespace ElderGuard.Plugins
{
    /// <summary>
    /// Configuration for the Console anti-scam plugin.
    /// </summary>
    public interface IElderGuardConsoleAntiScamPluginConfig
    {
        /// <summary>
        /// Should disable the developer tools.
        /// </summary>
        bool ShouldForceCloseCMD { get; set; }
    }

    /// <summary>
    /// The configuration for the Console-based anti-scam stuffs.
    /// </summary>
    public class ElderGuardConsoleAntiScamPluginConfig: IElderGuardConsoleAntiScamPluginConfig
    {
        public bool ShouldForceCloseCMD { get; set; }
    }

    /// <summary>
    /// A plugin which handles anti-scamming in relation to Console.
    /// </summary>
    public class ElderGuardConsoleAntiScamPlugin : ElderGuardPluginHost<ElderGuardConsoleAntiScamPluginConfig>
    {
        /// <summary>
        /// The program names we want to kill for console programs.
        /// </summary>
        protected static string[] COMMAND_LINE_PROGRAM_NAMES = new string[]
        {
            "cmd.exe",
            "powershell.exe",
            "pwsh.exe",
            "git-bash.exe",
            "cygwin.exe",
            "OpenConsole.exe"
        };

        /// <summary>
        /// The configuration for this plugin.
        /// </summary>
        protected IElderGuardConsoleAntiScamPluginConfig? pluginConfig { get; set; }

        /// <summary>
        /// The plugin information which defines this functionality.
        /// </summary>
        /// <returns></returns>
        public override IElderGuardPluginInformation GetPluginInformation()
        {
            return new ElderGuardPluginInformation()
            {
                DisplayName = "Console Anti-Scam",
                PluginName = "ConsoleAntiScam",
                Authors = new List<IElderGuardPluginInformationAuthor>()
                {
                    new ElderGuardPluginInformationAuthor()
                    {
                        GivenName = "Jordàn C. (Grimorio)",
                        EmailAddress = "grimorio@riseup.net"
                    }
                },
            };
        }

        /// <summary>
        /// The configuration for this item.
        /// </summary>
        /// <param name="configItem"></param>
        public override void LoadPluginConfiguration(object configSettings)
        {

            PropertyInfo? forceCloseCMD = configSettings?.GetType().GetProperty("ShouldForceCloseCMD");

            if (configSettings == null || forceCloseCMD == null)
            {
                return;
            }

            object? forceCloseCMDSettingValue = forceCloseCMD.GetValue(configSettings, null);

            if (forceCloseCMDSettingValue == null)
            {
                return;
            }

            this.pluginConfig = new ElderGuardConsoleAntiScamPluginConfig()
            {
                ShouldForceCloseCMD = (bool)forceCloseCMDSettingValue,
            };

            this.OnPluginConfigLoaded();
        }

        /// <summary>
        /// Registers stuff to be forcibly closed.
        /// </summary>
        protected void RegisterForceClose()
        {
            this.pluginInterop.useProcessManager()
                .AutoKillProcessesByName(ElderGuardConsoleAntiScamPlugin.COMMAND_LINE_PROGRAM_NAMES.ToList<string>());
        }

        /// <summary>
        /// Handles changes of the configuration of the plugin.
        /// </summary>
        protected void handleConfigChange()
        {
            if (this.pluginConfig != null && this.pluginConfig.ShouldForceCloseCMD)
            {
                this.RegisterForceClose();
            }
        }

        protected void OnPluginConfigLoaded()
        {
            this.handleConfigChange();
        }

        public override ElderGuardPluginState GetPluginState()
        {
            throw new NotImplementedException();
        }

        public override void LoadPluginConfiguration(ElderGuardConsoleAntiScamPluginConfig pluginConfig)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialises the anti-scam plugin.
        /// </summary>
        /// <param name="pluginInterOp"></param>
        public ElderGuardConsoleAntiScamPlugin(
            ElderGuardPluginInterOp pluginInterOp
        ) : base(pluginInterOp) {
        }
    }
}
