﻿using ElderGuard.PluginInterOp;
using ElderGuard.PluginInterOp.Enums;
using ElderGuard.PluginInterOp.Interfaces;
using System;
using System.Collections.Generic;

namespace ElderGuard.PluginInterOp.Concretes
{
    public class ElderGuardPluginInformationAuthor : IElderGuardPluginInformationAuthor
    {
        public string GivenName { get; set; }
        public string EmailAddress { get; set; }
    }

    public class ElderGuardPluginInformation : IElderGuardPluginInformation
    {
        public string? DisplayName { get; set; }
        public string? PluginName { get; set; }
        public List<IElderGuardPluginInformationAuthor>? Authors { get; set; }
    }
}

public class ElderGuardPlugin<TConfigurationType> : IElderGuardPlugin<TConfigurationType>
{
    public IElderGuardPluginInformation GetPluginInformation()
    {
        throw new NotImplementedException();
    }

    public ElderGuardPluginState GetPluginState()
    {
        throw new NotImplementedException();
    }

    public void Initialise(TConfigurationType pluginConfig, IElderGuardPluginInterOp interOp)
    {
        throw new NotImplementedException();
    }

    public void LoadPluginConfiguration(TConfigurationType pluginConfig)
    {
        throw new NotImplementedException();
    }

    public void LoadPluginConfiguration(object pluginConfig)
    {
        throw new NotImplementedException();
    }
}

