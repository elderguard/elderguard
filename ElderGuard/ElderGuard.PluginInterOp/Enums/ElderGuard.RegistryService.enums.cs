﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElderGuard.PluginInterOp.Enums
{
    /// <summary>
    /// The 
    /// </summary>
    public enum ElderGuardRegistryKeyLocation
    {
        User,
        sMachine,
    }

    /// <summary>
    /// Wrapper enum to prevent Win32 library from being imported in its entirety from Win32.Registry.
    /// </summary>
    public enum ElderGuardRegistryValueKind
    {
        None = -1,
        Unknown = 0,
        String = 1,
        ExpandString = 2,
        Binary = 3,
        DWord = 4,
        MultiString = 7,
        QWord = 11,
    }
}
