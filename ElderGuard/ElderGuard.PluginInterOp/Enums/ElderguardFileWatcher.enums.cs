﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Enums
{
    /// <summary>
    /// The type of event that is being fired.
    /// </summary>
    public enum ElderGuardFileWatcherEventType
    {
        Changed,
        Created,
        Deleted,
        Renamed,
        Error,
    }
}
