﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Dem' enums, yo.
/// </summary>
namespace ElderGuard.Enums
{
    /// <summary>
    /// The type of event that happened.
    /// </summary>
    public enum ElderGuardProcessManagerEventType
    {
        ProcessStarted,
        ProcessStopped,
        ProcessModified,
    }
}
