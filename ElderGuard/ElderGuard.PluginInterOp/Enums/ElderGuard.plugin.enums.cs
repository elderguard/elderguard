﻿namespace ElderGuard.PluginInterOp.Enums
{
    /// <summary>
    /// The state of the given plugin.
    /// </summary>
    public enum ElderGuardPluginState
    {
        IdleNotLoaded,
        InitFailedInvalidConfig,
        InitConfigLoaded,
        InitStarting,
        InitConcluded,
        PluginOperating,
        PluginInError,
    }
}
