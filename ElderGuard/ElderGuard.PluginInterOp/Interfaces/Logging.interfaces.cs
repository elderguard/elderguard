﻿using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Interfaces.Logging
{

    public interface ILoggingMessage<TExtraType> : ILoggingMessage {
        TExtraType context { get; set; }
    }

    public interface ILoggingMessage
    {
        LogEventLevel level { get; set; }
        string logMessage { get; set; }
    }
}
