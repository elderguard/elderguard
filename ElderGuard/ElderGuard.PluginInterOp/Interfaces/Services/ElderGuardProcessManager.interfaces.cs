﻿using ElderGuard.Enums;
namespace ElderGuard.Interfaces.Services
{
    /// <summary>
    /// The typed version of a process manager event.
    /// </summary>
    /// <typeparam name="TEventContextType"></typeparam>
    public interface IElderguardProcessManagerEvent<TEventContextType>: IElderGuardProcessManagerEvent
    {
        /// <summary>
        /// The context for this event change, if typed.
        /// </summary>
        new TEventContextType Context { get; set; }  
    }

    /// <summary>
    /// An un-typed version of a process manager event.
    /// </summary>
    public interface IElderGuardProcessManagerEvent
    {
        ElderGuardProcessManagerEventType? EventType { get; set; }
        object? Context { get; set; }
    }
}
