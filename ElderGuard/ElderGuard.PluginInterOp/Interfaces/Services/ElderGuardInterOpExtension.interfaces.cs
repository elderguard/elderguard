﻿using ElderGuard.Interfaces.Services;
using ElderGuard.PluginInterOp.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElderGuard.PluginInterOp.Interfaces.Services
{
    /// <summary>
    /// The base interop extension for the interop services.
    /// </summary>
    public interface IElderGuardInterOpExtensionBase:   IElderGuardSoftwareInterOpExtension, 
                                                        IElderGuardRegistryInterOpExtension,
                                                        IElderGuardFileWatcherInterOpExtension
    {

    }

    /// <summary>
    /// The extension for the software management service. 
    /// </summary>
    public interface IElderGuardSoftwareInterOpExtension
    {
        /// <summary>
        /// Refresh installed software
        /// </summary>
        public void refreshInstalledSoftware();

        /// <summary>
        /// Checks the software requested is installed.
        /// </summary>
        /// <param name="softwareName"></param>
        /// <returns></returns>
        public bool isSoftwareInstalled(string softwareName);
    }

    /// <summary>
    /// The ElderGuard registry operation extension.
    /// </summary>
    public interface IElderGuardRegistryInterOpExtension
    {
        /// <summary>
        /// Adds a key to a given registry path.
        /// </summary>
        /// <param name="SubKeyLocation"></param>
        /// <param name="location"></param>
        /// <param name="keyValueName"></param>
        /// <param name="value"></param>
        /// <param name="valueKind"></param>
        /// <returns></returns>
        public bool AddKeyToRegistryPath(
            string SubKeyLocation,
            ElderGuardRegistryKeyLocation location,
            string keyValueName, object value,
            ElderGuardRegistryValueKind valueKind
        );
    }

    /// <summary>
    /// The ElderGuard file watcher operation extension.
    /// </summary>
    public interface IElderGuardFileWatcherInterOpExtension
    {
        public void AddWatch(IElderGuardFileWatcherConfiguration configInstance);
    }
}
