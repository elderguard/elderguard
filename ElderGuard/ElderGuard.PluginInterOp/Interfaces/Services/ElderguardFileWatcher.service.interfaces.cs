﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElderGuard.Enums;

namespace ElderGuard.Interfaces.Services
{
    /// <summary>
    /// The path item for watching files.
    /// </summary>
    public interface IElderguardFileWatcherConfigurationPathItem
    {
        /// <summary>
        /// The path to watch as a string.
        /// </summary>
        public string? pathAsString { get; set; }

        /// <summary>
        /// The glob of files to watch.
        /// </summary>
        public string? glob { get; set; }
    }

    /// <summary>
    /// The configuration of the file watcher service.
    /// </summary>
    public interface IElderGuardFileWatcherConfiguration
    {
        public List<IElderguardFileWatcherConfigurationPathItem>? PathsToWatch { get; set; }
    }

    public interface IElderGuardianFileWatcherEvent
    {
        /// <summary>
        /// The arguments passed from the watcher.
        /// </summary>
        public FileSystemEventArgs? args { get; set; }
    }
}
