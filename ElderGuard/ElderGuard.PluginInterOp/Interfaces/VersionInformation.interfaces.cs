﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Interfaces
{
    public interface IVersionInformation
    {
        public string SoftwareName { get; set; }
        public string SoftwareSemanticVersion { get; set; }
        public string SoftwareReleaseName { get; set; }
        public string SoftwareRevision { get; set; }
    }
}
