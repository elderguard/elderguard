﻿using ElderGuard.PluginInterOp.Enums;
using System.Collections.Generic;


namespace ElderGuard.PluginInterOp.Interfaces
{
    public interface IElderGuardPluginInformationAuthor
    {
        public string? GivenName { get; set; }
        public string? EmailAddress { get; set; }
    }

    /// <summary>
    /// Information about a plugin.
    /// </summary>
    public interface IElderGuardPluginInformation
    {
        public string? DisplayName { get; set; }
        public string? PluginName { get; set; }
        public List<IElderGuardPluginInformationAuthor>? Authors { get; set; }
    }

    /// <summary>
    /// The base plugin interfaces
    /// </summary>
    public interface IElderGuardPlugin
    {
        /// <summary>
        /// Gets the state of the given plugin.
        /// </summary>
        /// <returns></returns>
        public ElderGuardPluginState GetPluginState();

        /// <summary>
        /// Gets the plugin information.
        /// </summary>
        /// <returns></returns>
        public IElderGuardPluginInformation GetPluginInformation();

        /// <summary>
        /// The main entry where the plugin interop calls to the plugin to start up.
        /// </summary>
        public void LoadPluginConfiguration(object pluginConfig);
    }

    /// <summary>
    /// An ElderGuard plugin.
    /// </summary>
    public interface IElderGuardPlugin<TConfigurationType> : IElderGuardPlugin
    {
        /// <summary>
        /// The main entry where the plugin interop calls to the plugin to start up.
        /// </summary>
        public void LoadPluginConfiguration(TConfigurationType pluginConfig);
    }
}
