﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Tasks
{
    public enum IElderTaskStage
    {
        /// <summary>
        /// When the application loads
        /// </summary>
        OnAppBoot,

        /// <summary>
        /// When the system itself boots up.
        /// </summary>
        OnSystemBoot,

        /// <summary>
        /// Something changed in the file system, for instance config was changed, file was added.
        /// </summary>
        OnFileSystemChange,

        /// <summary>
        /// When the 
        /// </summary>
        OnNetworkConnection,
    }

    /// <summary>
    /// A task which elder performs.
    /// </summary>
    public interface IElderTask {
        /// <summary>
        /// The stage that this task occurs at.
        /// </summary>
        IElderTaskStage occursAtStage { get; set; }
    }
}
