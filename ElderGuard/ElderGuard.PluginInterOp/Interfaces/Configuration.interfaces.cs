﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderGuard.Interfaces
{
    public interface IElderguardConfigurationPluginConfigItem<TPluginSettingsType>
    {
        TPluginSettingsType pluginSettingsType { get; set; }
    }

    public interface IElderGuardConfigurationPluginConfigItem
    {
        string? pluginName { get; set; }
        object? pluginSettings { get; set; }
    }

    /// <summary>
    /// The configuration for the configuration items.
    /// </summary>
    public interface IElderGuardConfiguration
    {
        IElderGuardConfigurationPluginConfigItem[]? PluginConfigurationItems { get; set; }
    }
}
