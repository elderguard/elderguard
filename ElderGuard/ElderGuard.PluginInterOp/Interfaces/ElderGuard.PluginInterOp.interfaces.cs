﻿using ElderGuard.PluginInterOp.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElderGuard.PluginInterOp
{
    /// <summary>
    /// Definition of the interop shared between plugins and the interop itself.
    /// This allows us to interchange stuff.
    /// </summary>
    public interface IElderGuardPluginInterOp
    {
        /// <summary>
        /// Registers a plugin with the interop.
        /// </summary>
        /// <param name="plugin"></param>
        public void registerPlugin(IElderGuardPlugin<object> plugin);
    }
}
