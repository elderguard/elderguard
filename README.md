![ElderGuard](Logo.png)
> "They've got all my money, and if I don't send them this, I'll lose all of it. That's all I have to live on.."

> **An American Retiree, fearful of losing his entire retirement savings from scammers, as heard on the YouTube channel "Jim Browning"**

# For general public
## What is ElderGuard
ElderGuard is software that is designed to be installed by a **technical user** for an **elderly or non-technical user**, and puts protections in place in an attempt to prevent fraud which occurs against elderly computer users.

## What is Anti-Scamware?
Software that is designed to prevent scammers from taking advantage of vulnerable people and taking money from them which they can ill-afford to lose.

## What does it do?
When installed on a user's computer, it will run in the background and prevent various actions from occuring which are commonly used by scammers but not used by elderly or low-computer-literacy users.

For instance, programs such as the Command-Line Prompt, Event Viewer, are often used by scammers to convince users their computer has been compromised and then use this fear to swindle users into paying for services they do not need, or into handing over control of their PC to people who will convince them to send money.

## How can I use it?
Install on someone's PC who is vulnerable to fraud. A relative, friend, acquaintaince, and then leave it on their PC. The software is designed to work without much or any configuration, and is designed to try and prevent common tricks which lead to lots of money being lost by people who just cannot afford it.

# For developers
ElderGuard is a Windows .NET CORE 5 App written with Avalonia UI Components. It is designed to be a stand-alone app that plays nicely with antivirus and firewalls, and that is designed to prevent scamming attack-vectors as often scammers do not do malicious code injection or stuff antivirus would pick up, but use a victim's PC to trick victims into giving away their hard-earned savings and pensions. 

ElderGuard is designed to shut down as many of these paths that scammers use, without interfering too much or at all in the elderly user's average daily use of their machine.

## Features
* Semi-modular architecture (moving to MEF DLL modules with interop)
* IoC/Container-based (Services and resources using AutoFac)
* Services
    * FileWatcherService - Listens to changes in file folders (i.e TXT files on desktop, etc.)
    * RegistryService - Handles update and watch of registry changes (i.e. Trying to re-enable developer tools)
    * SoftwareManagerService - Handles updates to installed software and force-uninstall of unwanted software (TeamViewer, LogMeIn, etc.)

# Contributing
Anyone is welcome to help, in any way. Some suggestions:

## Support
* If you want to support me, contact me.

## Individuals
1. Help us design a fancy-looking UI that will help put users at ease
2. Are you good at crypto? Help us secure our software and its configuration.
3. Extend our code to add more features
4. Document scammer attack vectors to help developers extend features to combat these scumbags.
5. Fix bugs.

## Antivirus vendors
* Cooperation is always appreciated. Help us make your lives easier, and we will work with you.
* Listing our software as trusted would go a long way & certification with your software would help, too.